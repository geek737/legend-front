var btn = $('#button-top');
var scrollToTopBtn = document.getElementById("button-top")
var rootElement = document.documentElement
$(window).on("scroll", function() {
  if ($(window).scrollTop() > 300) {
    btn.show();
  } else {
    btn.hide();
  }
});
function scrollToTop() {
  // Scroll to top logic
  rootElement.scrollTo({
    top: 0,
    behavior: "smooth"
  })
}
scrollToTopBtn.addEventListener("click", scrollToTop)
/**
   * ********************************
   *            Card Script
   *  *******************************
   * */  
  
 const next = document.querySelector('#next')
 const prev = document.querySelector('#prev')

 function handleScrollNext (direction) {
     const cards = document.querySelector('.con-cards')
     cards.scrollLeft = cards.scrollLeft += window.innerWidth / 2 > 600 ? window.innerWidth / 2 : window.innerWidth - 100
 }
 function handleScrollPrev (direction) {
     const cards = document.querySelector('.con-cards')
     cards.scrollLeft = cards.scrollLeft -= window.innerWidth / 2 > 600 ? window.innerWidth / 2 : window.innerWidth - 100
 }

 next.addEventListener('click', handleScrollNext)
 prev.addEventListener('click', handleScrollPrev)


 /**
  * Burger menu click
  */

 $('#switchNav').on('click', function(){
  $('#navAdaptive__starsField').css({"visibility" : "visible", "opacity" : "1"});
  $('.navAdaptive').css({"right" : "0px", "opacity" : "1"});
  $('button.hamburger').addClass('is-active');
 });

 $('#closeAdaptive').on('click', function(){
  $('#navAdaptive__starsField').css({"visibility" : "hidden", "opacity" : "0"});
  $('.navAdaptive').css({"right" : "-300pxpx", "opacity" : "0"});
  $('button.hamburger').removeClass('is-active');
 });

 /**
  * PLay button
  */
$('#play__home').on('click', function(){
  $('#watchVideo').removeClass('d-none');
});

$('button.window__close').on('click', function(){
  $('#watchVideo').addClass('d-none');
});

/**
 * Load
 */


$(window).on("load",function(){
  window.scrollTo({ top: 0});
});


$('.button-close-video').on('click', function(){
  $(".video-wrapper").fadeIn(100).delay(100).fadeOut("slow");
  $('body').css("overflow", "");
});